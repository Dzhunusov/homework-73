const express = require('express');
const app = express();
const PORT = 8000;

app.get("/:name", (req, res) => {
  res.send(req.params.name);
});

app.listen(PORT, () => {
  console.log("Server running at http://localhost:8000");
})