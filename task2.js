const express = require('express');
const app = express();
const PORT = 8000;

app.get("/", (req, res) => {
  res.send("HEllo my Friends!");
});

app.listen(PORT, () => {
  console.log("Server running at http://localhost:8000");
});